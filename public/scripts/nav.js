/* =================== Toggle Nav =================== */
var toggleBtn = document.getElementById('toggleNav');
var nav = document.getElementById("nav");
var state = '1';
function expand() {
	nav.style.width = "250px";
	state = '2';
}
function collapse() {
	nav.style.width = "96px";
	state = '1';
}
function toggle() {
	if(state === "1") {
		expand();
	} else {
		collapse();
	}
}
toggleBtn.onclick = toggle;
function frame() {
	if(state === '1') {
		toggleBtn.innerHTML = '&rightarrow;'
	} else {
		toggleBtn.innerHTML = '&leftarrow;';
	}
}
setInterval(frame, 0);
/* =================== Update Game =================== */
var embed = document.getElementById('embed');
document.addEventListener('click', function(e) {
  var elem = e.target;
  var url = elem.dataset.url;
  if(!url) {
  	url = elem.parentElement.dataset.url;
  }
  if(elem.dataset.url || elem.parentElement.dataset.url) {
    embed.src = url;
  }
});