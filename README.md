# QAG77
This is a games website containing unblocked copies of web-based games.
# How To Use
## GitLab Pages
**Step 1:** Make sure you are connected to the internet  
**Step 2:** Go to [https://qag77.gitlab.io/qag77](https://qag77.gitlab.io/qag77)  
## Local
**Step 1:** Go to the [releases](https://github.com/Gamerboss3094/QAG77/releases)  
**Step 2:** Download the [latest release](https://github.com/Gamerboss3094/QAG77/releases/latest)  
**Step 3:** Extract QAG77.zip  
**Step 4:** Open the extracted folder  
**Step 5:** Open index.html in your web browser  
# Changelog
## [1.7.0] - 2024-05-15 to 2024-05-07 (I published bits of the update without realizing it)
### Added
- Added more games

### Changed

### Patched

## [1.6.0] - 2024-05-07
### Added
- Added a few games

### Changed

### Patched
- Fixed sm64 completely

## [1.5.0] - 2024-04-29
### Added

### Changed
- Changed the topbar to a sidebar

### Patched

## [1.4.0] - 2024-04-25
### Added
- GitLab support! 

### Changed

### Patched

## [1.3.1] - 2024-04-12
### Added

### Changed
- updated n-gon

### Patched
- Fixed sm64 somewhat

## [1.3.0] - 2024-04-11
### Added
- Added 2 more games (Tetris and Super Mario 64)

### Changed
- Removed Slope (Because Unity's WebGL does not run on local system)

### Patched

## [1.2.0] - 2024-03-25
### Added
- added gameMods folder
- Added link to repository

### Changed
- Updated n-gon

### Patched

## [1.1.0] - 2024-03-22
### Added
- Pause on focus lost (n-gon)
- New color scheme

### Changed
- New update system

### Patched

## [1.0.0] - 2024-03-13
- Release
